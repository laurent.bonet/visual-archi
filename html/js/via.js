// via.js

var VIA_DOC;
var PAN_ZOOM; // Object managing pan and zoom see https://github.com/ariutta/svg-pan-zoom
var PAN_ZOOM_FLAG;
var SLIDES_LIST = []; // Array that contain the slides (views declared in svg via:slides attribute)
var SLIDES_INDEX = 0; // Index stroring current slides number
var KEYCODE_NEXT_PAGE = 39;
var KEYCODE_PREVIOUS_PAGE = 37;
var KEYCODE_BLANK = 32;
var modal;

function init() {
  console.log("Init Function");
  initPanZoom();
  initModal();
  adaptSvgContainer();
  //initControl();
  //initLayersControl();
  //initViewsControl();
  //initSlidesControl();
  initSvgContainerInteractivity();
  initObjectInteractivity();

  //console.log("json " + viaJson.slides.slide2.layers);
  //$.each(viaJson.slides.slide2.layers, function(index, value) {
  //  console.log(index + " - " + value);
  //});
  var xmlDoc = $.parseXML($("#viaXml").html());
  VIA_DOC = $(xmlDoc);
  VIA_DOC.find("slide").each(function(item, el) {
    SLIDES_LIST[item] = $(el).attr("id");
  });
  //console.log("List of  Slides");
  //for (i = 0; i < SLIDES_LIST.length; i++) {
  //  console.log("- " + SLIDES_LIST[i]);
  //}
  navigateSlide("first");
}

function initControl() {
  $("#controlBtn").click(function() {
    console.log("foo");
    if ($("#control").css("display") == "none") {
      $("#control").css({
        display: "inline-block",
        witdh: "50%",
        background: "LightGray"
      });
    } else {
      $("#control").css("display", "none");
    }
  });
  $("#animBtn").click(function(event) {
    //console.log("animButton");
    event.stopPropagation();
    animatedViewPanZoom("slide3");
  });
}

function adaptSvgContainer() {
  //adapt svg container size to have the same ratio than svg content
  var clientHeight = document.body.clientHeight;
  var clientWidth = document.body.clientWidth;
  var svgWidth = PAN_ZOOM.getSizes().viewBox.width;
  var svgHeight = PAN_ZOOM.getSizes().viewBox.height;
  if (clientWidth / clientHeight > svgWidth / svgHeight) {
    h = clientHeight;
    w = Math.round((h * svgWidth) / svgHeight);
  } else {
    w = clientWidth;
    h = Math.round((w * svgHeight) / svgWidth);
  }
  $("#svgContainer").css({
    width: w + "px",
    height: h + "px"
  });
}

function initPanZoom() {
  console.log("initPanZoom");
  var svgElement = document.querySelector("svg");
  PAN_ZOOM = svgPanZoom(svgElement, {
    zoomEnabled: true,
    controlIconsEnabled: false,
    fit: true,
    center: true,
    minZoom: 0.1,
    maxZoom: 100
  });
}

function initObjectInteractivity() {
  $(".viaInteract").mousedown(function() {
    event.stopPropagation();
  });
  $(".viaInteract").mouseup(function(event) {
    event.stopPropagation();
    var target = event.target;
    while (target.getAttribute("class") != "viaInteract") {
      target = target.parentElement;
    }
    console.log("> " + event.target.getAttribute("id"));
    console.log(">> " + target.getAttribute("id"));
    console.log(">>> " + target.getAttribute("via:id"));
    VIA_INFO.objects.forEach(function(item) {
      if (item.id == target.getAttribute("via:id")) {
        console.log(">>>> " + item.desc + " " + item.link);
        var modalDiv = document.getElementById("modal-text");
        //console.log(" target " + target);
        modalDiv.innerHTML =
          item.desc +
          '</br><a target="_blank" href="' +
          item.link +
          '">link</a>';
        modal.style.display = "block";
        /*
        var myWindow = window.open("", "Info", "width=200,height=100");
        myWindow.document.write(
          "<p>" +
            item.desc +
            "</p>" +
            '<a href="http://' +
            item.link +
            '">more info ...</a>'
        );
        */
      }
    });
  });
}

function initSvgContainerInteractivity() {
  $(window).on("resize", function() {
    console.log("window resize");
    adaptSvgContainer();
  });
  $("#svgContainer").mousedown(function() {
    PAN_ZOOM_FLAG = 0;
  });
  $("#svgContainer").mousemove(function() {
    PAN_ZOOM_FLAG = 1;
  });
  $("#svgContainer").mouseup(function() {
    if (PAN_ZOOM_FLAG == 0) {
      navigateSlide("next");
    }
  });
  $("body").keydown(function(event) {
    console.log(event.keyCode);
    if (event.keyCode == 49) {
      displaySlide("slide1");
    }
    if (event.keyCode == 50) {
      displaySlide("slide2");
    }
    if (event.keyCode == 51) {
      displaySlide("slide3");
    }
    if (event.keyCode == 52) {
      displaySlide("slide4");
    }
    if (event.keyCode == 73) {
      // i
      panZoomInfo();
    }
    if (event.keyCode == 67) {
      // c
      PAN_ZOOM.resize();
      PAN_ZOOM.fit();
      PAN_ZOOM.center();
    }
    if (event.keyCode == 84) {
      // t
      test();
    }

    if (event.keyCode == KEYCODE_BLANK) {
      navigateSlide("next");
    }
    if (event.keyCode == KEYCODE_NEXT_PAGE) {
      if (event.shiftKey == 1) {
        navigateSlide("last");
      } else {
        navigateSlide("next");
      }
    }
    if (event.keyCode == KEYCODE_PREVIOUS_PAGE) {
      if (event.shiftKey == 1) {
        navigateSlide("first");
      } else {
        navigateSlide("previous");
      }
    }
  });
}

function viewPanZoom(view) {
  PAN_ZOOM.resize();
  PAN_ZOOM.fit();
  PAN_ZOOM.center();
  var xViewCenter =
    parseFloat(view.attr("x")) + parseFloat(view.attr("width") / 2);
  var yViewCenter =
    parseFloat(view.attr("y")) + parseFloat(view.attr("height") / 2);
  var xBoxCenter = parseFloat(PAN_ZOOM.getSizes().viewBox.width / 2);
  var yBoxCenter = parseFloat(PAN_ZOOM.getSizes().viewBox.height / 2);
  PAN_ZOOM.panBy({
    x: parseFloat((xBoxCenter - xViewCenter) * PAN_ZOOM.getSizes().realZoom),
    y: parseFloat((yBoxCenter - yViewCenter) * PAN_ZOOM.getSizes().realZoom)
  });
  xFactor = PAN_ZOOM.getSizes().viewBox.width / view.attr("width");
  yFactor = PAN_ZOOM.getSizes().viewBox.height / view.attr("height");
  zoomFactor = Math.min(xFactor, yFactor);
  //console.log("zoomFactor " + zoomFactor);
  PAN_ZOOM.zoomBy(zoomFactor);
}

function test() {
  console.log("test");
  /*
  var box = { x: 350, y: 130, w: 100, h: 50 };
  panZoomCurrentTo(box, 2);
  */
  viaExport();
}

function animatedViewPanZoom(slide) {
  console.log("Slide = " + slide);
  target = viaJson.slides[slide];
  // Hide all layers then show relevant layers
  $(".viaLayer").each(function(index, el) {
    $(this).css("display", "none");
  });
  $.each(target.layers, function(index, el) {
    $('[via\\:id="' + target.layers[index] + '"]').css("display", "inline");
  });
}

function geoInfo(targetView) {
  console.log(
    "tx = " +
      Math.round(svgObjCenter(targetView).x) +
      " ty = " +
      Math.round(svgObjCenter(targetView).y)
  );
  console.log("Target Zoom Factor = " + targetZoomFactor(targetView));
}

function panZoomInfo() {
  console.log(
    "Pan -> x = " +
      Math.round(PAN_ZOOM.getPan().x) +
      " py = " +
      Math.round(PAN_ZOOM.getPan().y)
  );
  console.log("Zoom Factor -> " + Math.round(PAN_ZOOM.getZoom() * 100) / 100);
  console.log(
    "View Box -> w= " +
      Math.round(PAN_ZOOM.getSizes().viewBox.width) +
      " h = " +
      Math.round(PAN_ZOOM.getSizes().viewBox.height)
  );
  console.log(
    "Real Zoom -> " + Math.round(PAN_ZOOM.getSizes().realZoom * 100) / 100
  );
}

function viaExport() {
  alert("foo");
  const url =
    "https://script.google.com/macros/s/AKfycbwYlnSoBdaJ_sQ93CjyAACYUi3MnYU204asm8zA661Q11Gp4Jk/exec?cmd=export";

  fetch(url)
    .then(function(data) {
      alert(data);
      // Here you get the data to modify as you please
    })
    .catch(function(error) {
      alert(error);
      // If there is any error you will catch them here
    });
  /*
  fetch(url)
    .then(res => {
      alert res.text();
    })
    .then(data => {
      alert(data);
    });
    */
}

function initModal() {
  console.log("initModal");
  // Get the modal
  modal = document.getElementById("myModal");

  // Get the button that opens the modal
  //var btn = document.getElementById("animBtn");

  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

  // When the user clicks on the button, open the modal
  //btn.onclick = function() {
  //  modal.style.display = "block";
  //};

  // When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  };

  // When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };
}
