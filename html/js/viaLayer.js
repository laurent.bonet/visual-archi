function initLayersControl() {
  $(".viaLayer").each(function(index, element) {
    $("#layersControl").append(
      '<button class="layerButton" id="layerBtn' +
        $(this).attr("id") +
        '">' +
        $(this).attr("inkscape:label") +
        "</button>"
    );
  });
  $(".layerButton").on("click", function() {
    $(".slideButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    $(".viewButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    console.log("layerBtn clicked " + $(this).attr("id"));
    layerSelector =
      "#" +
      $(this)
        .attr("id")
        .substring(8);
    //substr to remove 'layerBtn' prefix
    if ($(layerSelector).css("display") == "none") {
      showLayer($(layerSelector));
    } else {
      hideLayer($(layerSelector));
    }
  });
}

function hideAllLayers() {
  //hide all layers based on viaLayer class + inkscape class
  $(".viaLayer").each(function(index, el) {
    $(this).css("display", "none");
  });
  $("[inkscape\\:groupmode=layer]").each(function(index, element) {
    $(this).css("display", "none");
  });
  /* TODO
  $("[inkscape\\:groupmode=layer]").each(function(index, element) {
    $(this).css("display", "none");
    layerSelector = "#layerBtn" + $(this).attr("id");
    //TODO : manage case of inkscape layer not being a viaLayer
    $(layerSelector).css("background", "White");
  });
  */
}

function showLayers(slide) {
  VIA_DOC.find("#" + slide)
    .find("layer")
    .each(function(item, el) {
      $('[via\\:id="' + $(el).text() + '"]').css("display", "inline");
    });
}

function showLayer(layer) {
  layerSelector = "#layerBtn" + layer.attr("id");
  $(layerSelector).css("background", "Gray");
  layer.css("display", "inline");
}

function hideLayer(layer) {
  layerSelector = "#layerBtn" + layer.attr("id");
  $(layerSelector).css("background", "White");
  layer.css("display", "none");
}
