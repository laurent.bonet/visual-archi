// viaPanZoom.js : functions related to svg display

function panZoomCurrentBox() {
  //geometrical characteristic of the current box
  var obj = {
    x: -PAN_ZOOM.getPan().x / PAN_ZOOM.getSizes().realZoom,
    y: -PAN_ZOOM.getPan().y / PAN_ZOOM.getSizes().realZoom,
    w: PAN_ZOOM.getSizes().viewBox.width / PAN_ZOOM.getZoom(),
    h: PAN_ZOOM.getSizes().viewBox.height / PAN_ZOOM.getZoom()
  };
  return obj;
}

function panZoomDelta(cBox, tBox) {
  //geometrical delta between a current box (cBox) and a target one (tBox)
  var obj = {
    dx: parseFloat(
      tBox.x + parseFloat(tBox.w / 2) - (cBox.x + parseFloat(cBox.w / 2))
    ),
    dy: parseFloat(
      tBox.y + parseFloat(tBox.h / 2) - (cBox.y + parseFloat(cBox.h / 2))
    ),
    dw: parseFloat(tBox.w - cBox.w),
    dh: parseFloat(tBox.h - cBox.h)
  };
  return obj;
}

function panZoomSteps(cBox, tBox, stepsNumber) {
  //array of intermediate boxes between a current box (cBox) and a target one (tBox)
  var delta = panZoomDelta(cBox, tBox);
  var steps = [];
  for (i = 0; i < stepsNumber; i++) {
    var box = {
      x: parseFloat(
        cBox.x +
          parseFloat(cBox.w / 2) +
          parseFloat(delta.dx / stepsNumber) * (i + 1) -
          (cBox.w + parseFloat(delta.dw / stepsNumber) * (i + 1)) / 2
      ),
      y: parseFloat(
        cBox.y +
          parseFloat(cBox.h / 2) +
          parseFloat(delta.dy / stepsNumber) * (i + 1) -
          (cBox.h + parseFloat(delta.dh / stepsNumber) * (i + 1)) / 2
      ),
      w: parseFloat(cBox.w + parseFloat(delta.dw / stepsNumber) * (i + 1)),
      h: parseFloat(cBox.h + parseFloat(delta.dh / stepsNumber) * (i + 1))
    };
    steps[i] = box;
  }
  return steps;
}

function panZoomTo(box) {
  //zoom and pan to the target specified box
  PAN_ZOOM.resize();
  PAN_ZOOM.fit();
  PAN_ZOOM.center();
  viewBox = PAN_ZOOM.getSizes().viewBox;
  realZoom = PAN_ZOOM.getSizes().realZoom;
  zoomFactor = Math.min(viewBox.width / box.w, viewBox.height / box.h);
  PAN_ZOOM.panBy({
    x: parseFloat(
      (parseFloat(viewBox.width / 2) - parseFloat(box.x + box.w / 2)) * realZoom
    ),
    y: parseFloat(
      (parseFloat(viewBox.height / 2) - parseFloat(box.y + box.h / 2)) *
        realZoom
    )
  });
  PAN_ZOOM.zoomBy(zoomFactor);
}

function panZoomCurrentTo(tBox, time) {
  //pan and zoom from the current box to the target one in a time expressed in s
  if (time == 0) {
    panZoomTo(tBox);
  } else {
    var animationStepTime = 10; // one frame per 10 ms
    var steps = (time * 1000) / animationStepTime;
    var step = 0;
    var cBox = panZoomCurrentBox();
    var stepsBox = panZoomSteps(cBox, tBox, steps);
  }

  var intervalID = setInterval(function() {
    if (step < steps) {
      panZoomTo(stepsBox[step]);
      step = step + 1;
    } else {
      clearInterval(intervalID);
    }
  }, animationStepTime);
}

function consoleBox(title, box) {
  // debug function displaying box x,y,w,h
  console.log(
    title +
      " " +
      Math.round(box.x) +
      " " +
      Math.round(box.y) +
      " " +
      Math.round(box.w) +
      " " +
      Math.round(box.h)
  );
}
