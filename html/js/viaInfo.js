var VIA_INFO = {
  objects: [
    {
      id: "kbo1",
      desc: "First Key Business Object",
      link: "http://www.airbus.com"
    },
    {
      id: "kbo2",
      desc: "Second Key Business Object",
      link: "http://www.lemonde.fr"
    },
    {
      id: "kbo3",
      desc: "Third Key Business Object",
      link: "http://www.wikipedia.com"
    }
  ]
};
