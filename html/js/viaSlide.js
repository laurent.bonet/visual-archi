function initSlidesControl() {
  slides = $("svg")
    .attr("via:slides")
    .split(";");
  $.each(slides, function(index, el) {
    $("#slidesControl").append(
      '<button class="slideButton" id="slideBtn' +
        $('[via\\:id="' + slides[index] + '"]').attr("id") +
        '" via:id="' +
        slides[index] +
        '">' +
        slides[index] +
        "</button>"
    );
  });

  $(".slideButton").on("click", function() {
    $(".slideButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    viewFocus($(this).attr("via:id"));
    $(this).css("background", "Gray");
  });
  navigateSlide("first");
}

function _navigateSlide(position) {
  //old
  //console.log("position = " + position + " SLIDES_INDEX = " + SLIDES_INDEX);
  $(".slideButton").each(function(index, element) {
    $(this).css("background", "White");
  });
  if (position == "first") {
    SLIDES_INDEX = 0;
  }
  if (position == "last") {
    SLIDES_INDEX = slides.length - 1;
  }
  if (position == "next") {
    if (SLIDES_INDEX < slides.length - 1) {
      SLIDES_INDEX = SLIDES_INDEX + 1;
    } else {
      SLIDES_INDEX = 0;
    }
  }
  if (position == "previous") {
    if (SLIDES_INDEX == 0) {
      SLIDES_INDEX = slides.length - 1;
    } else {
      SLIDES_INDEX = SLIDES_INDEX - 1;
    }
  }
  slideSelector =
    "#slideBtn" + $('[via\\:id="' + slides[SLIDES_INDEX] + '"]').attr("id");

  $(slideSelector).css("background", "Gray");
  viewFocus(slides[SLIDES_INDEX]);
}

function navigateSlide(position) {
  //navigate to slide according to position: first,last,next,previous
  if (position == "first") {
    SLIDES_INDEX = 0;
  }
  if (position == "last") {
    SLIDES_INDEX = SLIDES_LIST.length - 1;
  }
  if (position == "next") {
    if (SLIDES_INDEX < SLIDES_LIST.length - 1) {
      SLIDES_INDEX = SLIDES_INDEX + 1;
    } else {
      SLIDES_INDEX = 0;
    }
  }
  if (position == "previous") {
    if (SLIDES_INDEX == 0) {
      SLIDES_INDEX = SLIDES_LIST.length - 1;
    } else {
      SLIDES_INDEX = SLIDES_INDEX - 1;
    }
  }
  displaySlide(SLIDES_LIST[SLIDES_INDEX]);
}

function displaySlide(slide) {
  //hide all layers, display relevant one, pan zoom to the target
  //console.log("displaySlide " + slide);
  hideAllLayers();
  showLayers(slide);
  var target = slideToBox(slide);
  transition = VIA_DOC.find("#" + slide).attr("transition");
  panZoomCurrentTo(target, transition);
}

function slideToBox(slide) {
  viewId = VIA_DOC.find("#" + slide).attr("view");
  view = $('[via\\:id="' + viewId + '"]');
  var obj = {
    x: parseFloat(view.attr("x")),
    y: parseFloat(view.attr("y")),
    w: parseFloat(view.attr("width")),
    h: parseFloat(view.attr("height"))
  };
  return obj;
}
