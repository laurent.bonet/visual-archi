function initViewsControl() {
  $(".viaView").each(function(index, element) {
    $("#viewsControl").append(
      '<button class="viewButton" id="viewBtn' +
        $(this).attr("id") +
        '" via:id="' +
        $(this).attr("via:id") +
        '">' +
        $(this).attr("via:id") +
        "</button>"
    );
  });

  $(".viewButton").on("click", function() {
    $(".slideButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    viewFocus($(this).attr("via:id"));
  });
}

function viewFocus(viaId) {
  //console.log("viewFocus " + viaId);
  $(".viewButton").each(function(index, element) {
    $(this).css("background", "white");
  });
  viewSelector = "#viewBtn" + $('[via\\:id="' + viaId + '"]').attr("id");
  $(viewSelector).css("background", "Gray");
  hideAllLayers();
  var view = $('[via\\:id="' + viaId + '"]'); // Get svg Element through viaId attribute
  viewSelector = "#btnView" + view.attr("id");
  $(viewSelector).css("background", "Gray");
  var viewLayers = view.attr("via:layers").split(";"); // Get its Layers attribute and transform it to an array
  $.each(viewLayers, function(index, el) {
    showLayer($('[inkscape\\:label="' + viewLayers[index] + '"]'));
  });
  viewPanZoom(view);
}

function animatedViewFocus(viaId) {
  //console.log("animatedViewFocus " + viaId);
  $(".viewButton").each(function(index, element) {
    $(this).css("background", "white");
  });
  viewSelector = "#viewBtn" + $('[via\\:id="' + viaId + '"]').attr("id");
  $(viewSelector).css("background", "Gray");
  hideAllLayers();
  var view = $('[via\\:id="' + viaId + '"]'); // Get svg Element through viaId attribute
  viewSelector = "#btnView" + view.attr("id");
  $(viewSelector).css("background", "Gray");
  var viewLayers = view.attr("via:layers").split(";"); // Get its Layers attribute and transform it to an array
  $.each(viewLayers, function(index, el) {
    showLayer($('[inkscape\\:label="' + viewLayers[index] + '"]'));
  });
  animatedViewPanZoom(view);
}
