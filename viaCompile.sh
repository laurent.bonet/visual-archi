echo "viaCompile generate single html file"
echo "Param $1"
cat ./html/js/jquery-3.4.1.min.js ./html/js/svg-pan-zoom.min.js ./html/js/via.js ./html/js/viaPanZoom.js  ./html/js/viaLayer.js ./html/js/viaView.js ./html/js/viaSlide.js > ./html/js/compiledScript.js
cat ./html/htmlTemplate/html1 ./html/css/via.css ./html/htmlTemplate/html2 ./html/js/compiledScript.js ./html/htmlTemplate/html3 $1.xml ./html/htmlTemplate/html4 $1.svg ./html/htmlTemplate/html5 > $1.html
