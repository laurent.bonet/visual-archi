// via.js

var VIA_DOC;
var PAN_ZOOM; // Object managing pan and zoom see https://github.com/ariutta/svg-pan-zoom
var PAN_ZOOM_FLAG;
var SLIDES_LIST = []; // Array that contain the slides (views declared in svg via:slides attribute)
var SLIDES_INDEX = 0; // Index stroring current slides number
var KEYCODE_NEXT_PAGE = 39;
var KEYCODE_PREVIOUS_PAGE = 37;
var KEYCODE_BLANK = 32;

function init() {
  console.log("Init Function");
  initPanZoom();
  adaptSvgContainer();
  //initControl();
  //initLayersControl();
  //initViewsControl();
  //initSlidesControl();
  initSvgContainerInteractivity();
  initObjectInteractivity();

  //console.log("json " + viaJson.slides.slide2.layers);
  //$.each(viaJson.slides.slide2.layers, function(index, value) {
  //  console.log(index + " - " + value);
  //});
  var xmlDoc = $.parseXML($("#viaXml").html());
  VIA_DOC = $(xmlDoc);
  VIA_DOC.find("slide").each(function(item, el) {
    SLIDES_LIST[item] = $(el).attr("id");
  });
  //console.log("List of  Slides");
  //for (i = 0; i < SLIDES_LIST.length; i++) {
  //  console.log("- " + SLIDES_LIST[i]);
  //}
  navigateSlide("first");
}

function initControl() {
  $("#controlBtn").click(function() {
    console.log("foo");
    if ($("#control").css("display") == "none") {
      $("#control").css({
        display: "inline-block",
        witdh: "50%",
        background: "LightGray"
      });
    } else {
      $("#control").css("display", "none");
    }
  });
  $("#animBtn").click(function(event) {
    //console.log("animButton");
    event.stopPropagation();
    animatedViewPanZoom("slide3");
  });
}

function adaptSvgContainer() {
  //adapt svg container size to have the same ratio than svg content
  var clientHeight = document.body.clientHeight;
  var clientWidth = document.body.clientWidth;
  var svgWidth = PAN_ZOOM.getSizes().viewBox.width;
  var svgHeight = PAN_ZOOM.getSizes().viewBox.height;
  if (clientWidth / clientHeight > svgWidth / svgHeight) {
    h = clientHeight;
    w = Math.round((h * svgWidth) / svgHeight);
  } else {
    w = clientWidth;
    h = Math.round((w * svgHeight) / svgWidth);
  }
  $("#svgContainer").css({
    width: w + "px",
    height: h + "px"
  });
}

function initPanZoom() {
  console.log("initPanZoom");
  var svgElement = document.querySelector("svg");
  PAN_ZOOM = svgPanZoom(svgElement, {
    zoomEnabled: true,
    controlIconsEnabled: false,
    fit: true,
    center: true,
    minZoom: 0.1
  });
}

function initObjectInteractivity() {
  $(".viaInteract").mousedown(function() {
    event.stopPropagation();
  });
  $(".viaInteract").mouseup(function(event) {
    event.stopPropagation();
    var target = event.target;
    while (target.getAttribute("class") != "viaInteract") {
      target = target.parentElement;
    }
    console.log("> " + event.target.getAttribute("id"));
    console.log(">> " + target.getAttribute("id"));
    console.log(">>> " + target.getAttribute("via:id"));
    VIA_INFO.objects.forEach(function(item) {
      if (item.id == target.getAttribute("via:id")) {
        console.log(">>>> " + item.desc + " " + item.link);
        var myWindow = window.open("", "Info", "width=200,height=100");
        myWindow.document.write(
          "<p>" +
            item.desc +
            "</p>" +
            '<a href="http://' +
            item.link +
            '">more info ...</a>'
        );
      }
    });
  });
}

function initSvgContainerInteractivity() {
  $(window).on("resize", function() {
    console.log("window resize");
    adaptSvgContainer();
  });
  $("#svgContainer").mousedown(function() {
    PAN_ZOOM_FLAG = 0;
  });
  $("#svgContainer").mousemove(function() {
    PAN_ZOOM_FLAG = 1;
  });
  $("#svgContainer").mouseup(function() {
    if (PAN_ZOOM_FLAG == 0) {
      navigateSlide("next");
    }
  });
  $("body").keydown(function(event) {
    console.log(event.keyCode);
    if (event.keyCode == 49) {
      displaySlide("slide1");
    }
    if (event.keyCode == 50) {
      displaySlide("slide2");
    }
    if (event.keyCode == 51) {
      displaySlide("slide3");
    }
    if (event.keyCode == 52) {
      displaySlide("slide4");
    }
    if (event.keyCode == 73) {
      // i
      panZoomInfo();
    }
    if (event.keyCode == 67) {
      // c
      PAN_ZOOM.resize();
      PAN_ZOOM.fit();
      PAN_ZOOM.center();
    }
    if (event.keyCode == 84) {
      // t
      test();
    }

    if (event.keyCode == KEYCODE_BLANK) {
      navigateSlide("next");
    }
    if (event.keyCode == KEYCODE_NEXT_PAGE) {
      if (event.shiftKey == 1) {
        navigateSlide("last");
      } else {
        navigateSlide("next");
      }
    }
    if (event.keyCode == KEYCODE_PREVIOUS_PAGE) {
      if (event.shiftKey == 1) {
        navigateSlide("first");
      } else {
        navigateSlide("previous");
      }
    }
  });
}

function viewPanZoom(view) {
  PAN_ZOOM.resize();
  PAN_ZOOM.fit();
  PAN_ZOOM.center();
  var xViewCenter =
    parseFloat(view.attr("x")) + parseFloat(view.attr("width") / 2);
  var yViewCenter =
    parseFloat(view.attr("y")) + parseFloat(view.attr("height") / 2);
  var xBoxCenter = parseFloat(PAN_ZOOM.getSizes().viewBox.width / 2);
  var yBoxCenter = parseFloat(PAN_ZOOM.getSizes().viewBox.height / 2);
  PAN_ZOOM.panBy({
    x: parseFloat((xBoxCenter - xViewCenter) * PAN_ZOOM.getSizes().realZoom),
    y: parseFloat((yBoxCenter - yViewCenter) * PAN_ZOOM.getSizes().realZoom)
  });
  xFactor = PAN_ZOOM.getSizes().viewBox.width / view.attr("width");
  yFactor = PAN_ZOOM.getSizes().viewBox.height / view.attr("height");
  zoomFactor = Math.min(xFactor, yFactor);
  //console.log("zoomFactor " + zoomFactor);
  PAN_ZOOM.zoomBy(zoomFactor);
}

function test() {
  console.log("test");
  var box = { x: 350, y: 130, w: 100, h: 50 };
  panZoomCurrentTo(box, 2);
}

function animatedViewPanZoom(slide) {
  console.log("Slide = " + slide);
  target = viaJson.slides[slide];
  // Hide all layers then show relevant layers
  $(".viaLayer").each(function(index, el) {
    $(this).css("display", "none");
  });
  $.each(target.layers, function(index, el) {
    $('[via\\:id="' + target.layers[index] + '"]').css("display", "inline");
  });
}

function geoInfo(targetView) {
  console.log(
    "tx = " +
      Math.round(svgObjCenter(targetView).x) +
      " ty = " +
      Math.round(svgObjCenter(targetView).y)
  );
  console.log("Target Zoom Factor = " + targetZoomFactor(targetView));
}

function panZoomInfo() {
  console.log(
    "Pan -> x = " +
      Math.round(PAN_ZOOM.getPan().x) +
      " py = " +
      Math.round(PAN_ZOOM.getPan().y)
  );
  console.log("Zoom Factor -> " + Math.round(PAN_ZOOM.getZoom() * 100) / 100);
  console.log(
    "View Box -> w= " +
      Math.round(PAN_ZOOM.getSizes().viewBox.width) +
      " h = " +
      Math.round(PAN_ZOOM.getSizes().viewBox.height)
  );
  console.log(
    "Real Zoom -> " + Math.round(PAN_ZOOM.getSizes().realZoom * 100) / 100
  );
}
// viaPanZoom.js : functions related to svg display

function panZoomCurrentBox() {
  //geometrical characteristic of the current box
  var obj = {
    x: -PAN_ZOOM.getPan().x / PAN_ZOOM.getSizes().realZoom,
    y: -PAN_ZOOM.getPan().y / PAN_ZOOM.getSizes().realZoom,
    w: PAN_ZOOM.getSizes().viewBox.width / PAN_ZOOM.getZoom(),
    h: PAN_ZOOM.getSizes().viewBox.height / PAN_ZOOM.getZoom()
  };
  return obj;
}

function panZoomDelta(cBox, tBox) {
  //geometrical delta between a current box (cBox) and a target one (tBox)
  var obj = {
    dx: parseFloat(
      tBox.x + parseFloat(tBox.w / 2) - (cBox.x + parseFloat(cBox.w / 2))
    ),
    dy: parseFloat(
      tBox.y + parseFloat(tBox.h / 2) - (cBox.y + parseFloat(cBox.h / 2))
    ),
    dw: parseFloat(tBox.w - cBox.w),
    dh: parseFloat(tBox.h - cBox.h)
  };
  return obj;
}

function panZoomSteps(cBox, tBox, stepsNumber) {
  //array of intermediate boxes between a current box (cBox) and a target one (tBox)
  var delta = panZoomDelta(cBox, tBox);
  var steps = [];
  for (i = 0; i < stepsNumber; i++) {
    var box = {
      x: parseFloat(
        cBox.x +
          parseFloat(cBox.w / 2) +
          parseFloat(delta.dx / stepsNumber) * (i + 1) -
          (cBox.w + parseFloat(delta.dw / stepsNumber) * (i + 1)) / 2
      ),
      y: parseFloat(
        cBox.y +
          parseFloat(cBox.h / 2) +
          parseFloat(delta.dy / stepsNumber) * (i + 1) -
          (cBox.h + parseFloat(delta.dh / stepsNumber) * (i + 1)) / 2
      ),
      w: parseFloat(cBox.w + parseFloat(delta.dw / stepsNumber) * (i + 1)),
      h: parseFloat(cBox.h + parseFloat(delta.dh / stepsNumber) * (i + 1))
    };
    steps[i] = box;
  }
  return steps;
}

function panZoomTo(box) {
  //zoom and pan to the target specified box
  PAN_ZOOM.resize();
  PAN_ZOOM.fit();
  PAN_ZOOM.center();
  viewBox = PAN_ZOOM.getSizes().viewBox;
  realZoom = PAN_ZOOM.getSizes().realZoom;
  zoomFactor = Math.min(viewBox.width / box.w, viewBox.height / box.h);
  PAN_ZOOM.panBy({
    x: parseFloat(
      (parseFloat(viewBox.width / 2) - parseFloat(box.x + box.w / 2)) * realZoom
    ),
    y: parseFloat(
      (parseFloat(viewBox.height / 2) - parseFloat(box.y + box.h / 2)) *
        realZoom
    )
  });
  PAN_ZOOM.zoomBy(zoomFactor);
}

function panZoomCurrentTo(tBox, time) {
  //pan and zoom from the current box to the target one in a time expressed in s
  if (time == 0) {
    panZoomTo(tBox);
  } else {
    var animationStepTime = 10; // one frame per 10 ms
    var steps = (time * 1000) / animationStepTime;
    var step = 0;
    var cBox = panZoomCurrentBox();
    var stepsBox = panZoomSteps(cBox, tBox, steps);
  }

  var intervalID = setInterval(function() {
    if (step < steps) {
      panZoomTo(stepsBox[step]);
      step = step + 1;
    } else {
      clearInterval(intervalID);
    }
  }, animationStepTime);
}

function consoleBox(title, box) {
  // debug function displaying box x,y,w,h
  console.log(
    title +
      " " +
      Math.round(box.x) +
      " " +
      Math.round(box.y) +
      " " +
      Math.round(box.w) +
      " " +
      Math.round(box.h)
  );
}
function initLayersControl() {
  $(".viaLayer").each(function(index, element) {
    $("#layersControl").append(
      '<button class="layerButton" id="layerBtn' +
        $(this).attr("id") +
        '">' +
        $(this).attr("inkscape:label") +
        "</button>"
    );
  });
  $(".layerButton").on("click", function() {
    $(".slideButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    $(".viewButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    console.log("layerBtn clicked " + $(this).attr("id"));
    layerSelector =
      "#" +
      $(this)
        .attr("id")
        .substring(8);
    //substr to remove 'layerBtn' prefix
    if ($(layerSelector).css("display") == "none") {
      showLayer($(layerSelector));
    } else {
      hideLayer($(layerSelector));
    }
  });
}

function hideAllLayers() {
  //hide all layers based on viaLayer class + inkscape class
  $(".viaLayer").each(function(index, el) {
    $(this).css("display", "none");
  });
  $("[inkscape\\:groupmode=layer]").each(function(index, element) {
    $(this).css("display", "none");
  });
  /* TODO
  $("[inkscape\\:groupmode=layer]").each(function(index, element) {
    $(this).css("display", "none");
    layerSelector = "#layerBtn" + $(this).attr("id");
    //TODO : manage case of inkscape layer not being a viaLayer
    $(layerSelector).css("background", "White");
  });
  */
}

function showLayers(slide) {
  VIA_DOC.find("#" + slide)
    .find("layer")
    .each(function(item, el) {
      $('[via\\:id="' + $(el).text() + '"]').css("display", "inline");
    });
}

function showLayer(layer) {
  layerSelector = "#layerBtn" + layer.attr("id");
  $(layerSelector).css("background", "Gray");
  layer.css("display", "inline");
}

function hideLayer(layer) {
  layerSelector = "#layerBtn" + layer.attr("id");
  $(layerSelector).css("background", "White");
  layer.css("display", "none");
}
function initViewsControl() {
  $(".viaView").each(function(index, element) {
    $("#viewsControl").append(
      '<button class="viewButton" id="viewBtn' +
        $(this).attr("id") +
        '" via:id="' +
        $(this).attr("via:id") +
        '">' +
        $(this).attr("via:id") +
        "</button>"
    );
  });

  $(".viewButton").on("click", function() {
    $(".slideButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    viewFocus($(this).attr("via:id"));
  });
}

function viewFocus(viaId) {
  //console.log("viewFocus " + viaId);
  $(".viewButton").each(function(index, element) {
    $(this).css("background", "white");
  });
  viewSelector = "#viewBtn" + $('[via\\:id="' + viaId + '"]').attr("id");
  $(viewSelector).css("background", "Gray");
  hideAllLayers();
  var view = $('[via\\:id="' + viaId + '"]'); // Get svg Element through viaId attribute
  viewSelector = "#btnView" + view.attr("id");
  $(viewSelector).css("background", "Gray");
  var viewLayers = view.attr("via:layers").split(";"); // Get its Layers attribute and transform it to an array
  $.each(viewLayers, function(index, el) {
    showLayer($('[inkscape\\:label="' + viewLayers[index] + '"]'));
  });
  viewPanZoom(view);
}

function animatedViewFocus(viaId) {
  //console.log("animatedViewFocus " + viaId);
  $(".viewButton").each(function(index, element) {
    $(this).css("background", "white");
  });
  viewSelector = "#viewBtn" + $('[via\\:id="' + viaId + '"]').attr("id");
  $(viewSelector).css("background", "Gray");
  hideAllLayers();
  var view = $('[via\\:id="' + viaId + '"]'); // Get svg Element through viaId attribute
  viewSelector = "#btnView" + view.attr("id");
  $(viewSelector).css("background", "Gray");
  var viewLayers = view.attr("via:layers").split(";"); // Get its Layers attribute and transform it to an array
  $.each(viewLayers, function(index, el) {
    showLayer($('[inkscape\\:label="' + viewLayers[index] + '"]'));
  });
  animatedViewPanZoom(view);
}
function initSlidesControl() {
  slides = $("svg")
    .attr("via:slides")
    .split(";");
  $.each(slides, function(index, el) {
    $("#slidesControl").append(
      '<button class="slideButton" id="slideBtn' +
        $('[via\\:id="' + slides[index] + '"]').attr("id") +
        '" via:id="' +
        slides[index] +
        '">' +
        slides[index] +
        "</button>"
    );
  });

  $(".slideButton").on("click", function() {
    $(".slideButton").each(function(index, element) {
      $(this).css("background", "White");
    });
    viewFocus($(this).attr("via:id"));
    $(this).css("background", "Gray");
  });
  navigateSlide("first");
}

function _navigateSlide(position) {
  //old
  //console.log("position = " + position + " SLIDES_INDEX = " + SLIDES_INDEX);
  $(".slideButton").each(function(index, element) {
    $(this).css("background", "White");
  });
  if (position == "first") {
    SLIDES_INDEX = 0;
  }
  if (position == "last") {
    SLIDES_INDEX = slides.length - 1;
  }
  if (position == "next") {
    if (SLIDES_INDEX < slides.length - 1) {
      SLIDES_INDEX = SLIDES_INDEX + 1;
    } else {
      SLIDES_INDEX = 0;
    }
  }
  if (position == "previous") {
    if (SLIDES_INDEX == 0) {
      SLIDES_INDEX = slides.length - 1;
    } else {
      SLIDES_INDEX = SLIDES_INDEX - 1;
    }
  }
  slideSelector =
    "#slideBtn" + $('[via\\:id="' + slides[SLIDES_INDEX] + '"]').attr("id");

  $(slideSelector).css("background", "Gray");
  viewFocus(slides[SLIDES_INDEX]);
}

function navigateSlide(position) {
  //navigate to slide according to position: first,last,next,previous
  if (position == "first") {
    SLIDES_INDEX = 0;
  }
  if (position == "last") {
    SLIDES_INDEX = SLIDES_LIST.length - 1;
  }
  if (position == "next") {
    if (SLIDES_INDEX < SLIDES_LIST.length - 1) {
      SLIDES_INDEX = SLIDES_INDEX + 1;
    } else {
      SLIDES_INDEX = 0;
    }
  }
  if (position == "previous") {
    if (SLIDES_INDEX == 0) {
      SLIDES_INDEX = SLIDES_LIST.length - 1;
    } else {
      SLIDES_INDEX = SLIDES_INDEX - 1;
    }
  }
  displaySlide(SLIDES_LIST[SLIDES_INDEX]);
}

function displaySlide(slide) {
  //hide all layers, display relevant one, pan zoom to the target
  //console.log("displaySlide " + slide);
  hideAllLayers();
  showLayers(slide);
  var target = slideToBox(slide);
  transition = VIA_DOC.find("#" + slide).attr("transition");
  panZoomCurrentTo(target, transition);
}

function slideToBox(slide) {
  viewId = VIA_DOC.find("#" + slide).attr("view");
  view = $('[via\\:id="' + viewId + '"]');
  var obj = {
    x: parseFloat(view.attr("x")),
    y: parseFloat(view.attr("y")),
    w: parseFloat(view.attr("width")),
    h: parseFloat(view.attr("height"))
  };
  return obj;
}
